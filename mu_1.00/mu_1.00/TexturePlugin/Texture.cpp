// mu (myu) Max-Unity Interoperability Toolkit
// Ivica Ico Bukvic <ico@vt.edu> <http://ico.bukvic.net>
// Ji-Sun Kim <hideaway@vt.edu>
// Keith Wooldridge <kawoold@vt.edu>
// With thanks to Denis Gracanin

// Virginia Tech Department of Music
// DISIS Interactive Sound & Intermedia Studio
// Collaborative for Creative Technologies in the Arts and Design

// Copyright DISIS 2008.
// mu is distributed under the GPL license v3 (http://www.gnu.org/licenses/gpl.html)

#if _MSC_VER // this is defined when compiling with Visual Studio

// Windows: include OpenGL headers
#include <windows.h>
#include <gl/gl.h>
#define GL_UNSIGNED_INT_8_8_8_8 0x8035 // this is not present in GL 1.1 headers

#define EXPORT_API __declspec(dllexport) // Visual Studio needs annotating exported functions with this

#else

// Mac: include OpenGL headers
#include <OpenGL/gl.h>
#define EXPORT_API // XCode does not need annotating exported functions, so define is empty

#endif



// Link following functions C-style (required for plugins)
extern "C"
{

// The function we will call from Unity.
//
// We pass the texture ID, size current time to the plugin.
// The C++ plugin can then assign any kind of texture to OpenGL directly.
void EXPORT_API UpdateTexture( int textureID, int width, int height, unsigned char data[] )
{
	unsigned char* pixel;

	// 32 bit RGBA texture
	unsigned char* texData = new unsigned char[4 * width * height];

	for (int y=0;y<height;y++)
	{
		for (int x=0;x<width;x++)
		{
			//map and mirror horizontal
			pixel = texData + (y * width + (width - 1 - x)) * 4;
			//have to swap red and blue
			pixel[0] = data[0+(y * width + x) * 4];	// a
			pixel[1] = data[3+(y * width + x) * 4];	// r
			pixel[2] = data[2+(y * width + x) * 4];	// g
			pixel[3] = data[1+(y * width + x) * 4];	// b
		}
	}

	// Push GL state (we need to leave the bound texture like it was before)
	glPushAttrib (GL_TEXTURE_BIT);

	// Upload the texture to GL
	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexImage2D (GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8, texData);
	
	// Revert bind texture calls
	glPopAttrib ();
	
	delete[] texData;
}

} // end of export C block
