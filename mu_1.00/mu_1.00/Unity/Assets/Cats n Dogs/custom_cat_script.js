// mu (myu) Max-Unity Interoperability Toolkit
// Ivica Ico Bukvic <ico@vt.edu> <http://ico.bukvic.net>
// Ji-Sun Kim <hideaway@vt.edu>
// Keith Wooldridge <kawoold@vt.edu>
// With thanks to Denis Gracanin

// Virginia Tech Department of Music
// DISIS Interactive Sound & Intermedia Studio
// Collaborative for Creative Technologies in the Arts and Design

// Copyright DISIS 2008.
// mu is distributed under the GPL license v3 (http://www.gnu.org/licenses/gpl.html)

//This script is redundantly separated from the jitCustomEvents script
//(associated with the Main Camera object) as an example of how to cross-link
//different scripts as well as bridge C# and Javascript

var Cat : Rigidbody;

//IMPORTANT! Make function public so that it can be referenced from other scripts
public function Create_Cat() {
	var instance = Instantiate(Cat, Vector3(0.6568305, 4.370027, 2.624699), Quaternion.identity);
	instance.GetComponent(Rigidbody).AddForce((Random.value-0.5)*4, (Random.value-0.5)*4, (Random.value-0.5)*4);
}

//Uncomment below if you wish to spawn spheres by pressing keyboard keys within Unity3D
//function Update() {
//	if (Input.anyKeyDown) Create_Ball();	
//}
