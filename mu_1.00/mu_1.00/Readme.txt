mu (myu) Max-Unity Interoperability Toolkit
mu copyright DISIS 2008.
<http://disis.music.vt.edu>

Authors:
Ivica Ico Bukvic <ico@vt.edu>
Ji-Sun Kim <hideaway@vt.edu>
Keith Wooldridge <kawoold@vt.edu>
with thanks to Denis Gracanin

Virginia Tech Department of Music
DISIS Interactive Sound & Intermedia Studio
Collaborative for Creative Technologies in the Arts and Design

mu is distributed under the GPL license v3
<http://www.gnu.org/licenses/gpl.html>

Unity3D and Max/MSP/Jitter and their logos are trademarks of their
respective owners. netsend/netreceive externals copyright Olaf Matthes.

Before doing any testing, either disable the firewall or make sure to
allow both Max and Unity3D to create network sockets. To open the demo
program, start the appropriate executable found in the Unity/Tutorial
Binares/ folder, followed by the mu_tutorial patch found in the Max
folder.

For additional info on how to run the demo see mu_tutorial patch in the
Max folder.

For the Unity3D texture plugin source, see TexturePlugin folder.

FAQ
Q: What does mu do?
A: See mu_tutorial patch.

Q: I don't know what to do.
A: See mu_tutorial patch.

Q: I cannot see the texture on Win32 platform. What's wrong?
A: Yep, you guessed it right--see mu_tutorial patch. ;-)
