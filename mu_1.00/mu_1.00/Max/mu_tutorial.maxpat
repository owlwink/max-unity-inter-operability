{
	"patcher" : 	{
		"fileversion" : 1,
		"rect" : [ 14.0, 59.0, 686.0, 334.0 ],
		"bglocked" : 0,
		"defrect" : [ 14.0, 59.0, 686.0, 334.0 ],
		"openrect" : [ 0.0, 0.0, 0.0, 0.0 ],
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 0,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 0,
		"toolbarvisible" : 1,
		"boxanimatetime" : 200,
		"imprint" : 0,
		"metadata" : [  ],
		"boxes" : [ 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "Copyright DISIS 2008. See Readme for additional license info.",
					"fontname" : "Arial",
					"fontsize" : 9.0,
					"numinlets" : 1,
					"patching_rect" : [ 179.0, 206.0, 296.0, 17.0 ],
					"id" : "obj-1",
					"numoutlets" : 0
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "newobj",
					"text" : "p Max->Unity_Texture",
					"fontname" : "Arial",
					"fontsize" : 9.0,
					"numinlets" : 0,
					"patching_rect" : [ 505.0, 129.0, 113.0, 17.0 ],
					"id" : "obj-2",
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"rect" : [ 29.0, 481.0, 944.0, 486.0 ],
						"bglocked" : 0,
						"defrect" : [ 29.0, 481.0, 944.0, 486.0 ],
						"openrect" : [ 0.0, 0.0, 0.0, 0.0 ],
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 0,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 0,
						"toolbarvisible" : 1,
						"boxanimatetime" : 200,
						"imprint" : 0,
						"metadata" : [  ],
						"boxes" : [ 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "webcam playback (PC)",
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 42.0, 191.0, 129.0, 17.0 ],
									"id" : "obj-1",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "video off",
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 42.0, 150.0, 129.0, 17.0 ],
									"id" : "obj-2",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "number",
									"htextcolor" : [ 0.870588, 0.870588, 0.870588, 1.0 ],
									"triscale" : 0.9,
									"outlettype" : [ "int", "bang" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"maximum" : 3,
									"bgcolor" : [ 0.866667, 0.866667, 0.866667, 1.0 ],
									"patching_rect" : [ 324.0, 215.0, 35.0, 17.0 ],
									"id" : "obj-3",
									"numoutlets" : 2,
									"minimum" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "loadmess 256",
									"outlettype" : [ "" ],
									"fontname" : "Arial",
									"hidden" : 1,
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 190.0, 118.0, 72.0, 17.0 ],
									"id" : "obj-4",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "webcam playback (Mac)",
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 42.0, 180.0, 129.0, 17.0 ],
									"id" : "obj-5",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "video + chromakey",
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 42.0, 170.0, 129.0, 17.0 ],
									"id" : "obj-6",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "button",
									"outlettype" : [ "bang" ],
									"numinlets" : 1,
									"patching_rect" : [ 372.0, 160.0, 15.0, 15.0 ],
									"id" : "obj-7",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "Windows live camera input",
									"linecount" : 2,
									"fontname" : "Arial",
									"frgb" : [ 0.6, 0.4, 0.6, 1.0 ],
									"fontsize" : 9.0,
									"numinlets" : 1,
									"textcolor" : [ 0.6, 0.4, 0.6, 1.0 ],
									"patching_rect" : [ 573.0, 159.0, 68.0, 27.0 ],
									"id" : "obj-8",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "message",
									"text" : "open",
									"outlettype" : [ "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 2,
									"bgcolor" : [ 0.6, 0.4, 0.6, 1.0 ],
									"patching_rect" : [ 474.0, 185.0, 30.0, 15.0 ],
									"id" : "obj-9",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "jit.qt.grab 304 304",
									"outlettype" : [ "jit_matrix", "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 474.0, 213.0, 98.0, 17.0 ],
									"id" : "obj-10",
									"numoutlets" : 2
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "preset",
									"outlettype" : [ "preset", "int", "preset", "int" ],
									"spacing" : 2,
									"bubblesize" : 8,
									"numinlets" : 1,
									"margin" : 4,
									"patching_rect" : [ 26.0, 148.0, 17.0, 57.0 ],
									"id" : "obj-11",
									"numoutlets" : 4,
									"preset_data" : [ 										{
											"number" : 1,
											"data" : [ 5, "obj-43", "toggle", "int", 0, 5, "obj-33", "number", "int", 40, 5, "obj-24", "flonum", "float", 0.0, 5, "obj-23", "flonum", "float", 0.0, 5, "obj-22", "flonum", "float", 1.0, 5, "obj-21", "flonum", "float", 1.0, 5, "obj-19", "flonum", "float", 0.0, 5, "obj-17", "flonum", "float", 0.0, 7, "obj-14", "swatch", "list", 0, 255, 255, 5, "obj-3", "number", "int", 0 ]
										}
, 										{
											"number" : 2,
											"data" : [ 5, "obj-43", "toggle", "int", 1, 5, "obj-33", "number", "int", 40, 5, "obj-24", "flonum", "float", 0.0, 5, "obj-23", "flonum", "float", 0.0, 5, "obj-22", "flonum", "float", 0.0, 5, "obj-21", "flonum", "float", 0.0, 5, "obj-19", "flonum", "float", 0.0, 5, "obj-17", "flonum", "float", 0.0, 7, "obj-14", "swatch", "list", 38, 37, 65, 5, "obj-3", "number", "int", 1 ]
										}
, 										{
											"number" : 3,
											"data" : [ 5, "obj-43", "toggle", "int", 1, 5, "obj-33", "number", "int", 40, 5, "obj-24", "flonum", "float", 0.0, 5, "obj-23", "flonum", "float", 0.14902, 5, "obj-22", "flonum", "float", 0.145098, 5, "obj-21", "flonum", "float", 0.254902, 5, "obj-19", "flonum", "float", 0.5, 5, "obj-17", "flonum", "float", 1.0, 7, "obj-14", "swatch", "list", 38, 37, 65, 5, "obj-3", "number", "int", 1 ]
										}
, 										{
											"number" : 4,
											"data" : [ 5, "obj-43", "toggle", "int", 1, 5, "obj-33", "number", "int", 40, 5, "obj-24", "flonum", "float", 0.0, 5, "obj-23", "flonum", "float", 0.0, 5, "obj-22", "flonum", "float", 1.0, 5, "obj-21", "flonum", "float", 1.0, 5, "obj-19", "flonum", "float", 0.0, 5, "obj-17", "flonum", "float", 0.0, 7, "obj-14", "swatch", "list", 0, 255, 255, 5, "obj-3", "number", "int", 2 ]
										}
, 										{
											"number" : 5,
											"data" : [ 5, "obj-43", "toggle", "int", 1, 5, "obj-33", "number", "int", 40, 5, "obj-24", "flonum", "float", 0.0, 5, "obj-23", "flonum", "float", 0.0, 5, "obj-22", "flonum", "float", 1.0, 5, "obj-21", "flonum", "float", 1.0, 5, "obj-19", "flonum", "float", 0.0, 5, "obj-17", "flonum", "float", 0.0, 7, "obj-14", "swatch", "list", 0, 255, 255, 5, "obj-3", "number", "int", 3 ]
										}
 ]
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "unpack 0. 0. 0.",
									"outlettype" : [ "float", "float", "float" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 686.0, 249.0, 93.0, 17.0 ],
									"id" : "obj-12",
									"numoutlets" : 3
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "vexpr $i1/255.",
									"outlettype" : [ "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 686.0, 226.0, 77.0, 17.0 ],
									"id" : "obj-13",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "swatch",
									"outlettype" : [ "", "float" ],
									"compatibility" : 1,
									"numinlets" : 3,
									"patching_rect" : [ 686.0, 190.0, 128.0, 32.0 ],
									"id" : "obj-14",
									"numoutlets" : 2
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "suckah",
									"outlettype" : [ "" ],
									"compatibility" : 1,
									"numinlets" : 1,
									"patching_rect" : [ 686.0, 125.0, 80.0, 60.0 ],
									"id" : "obj-15",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "jit.pwindow",
									"outlettype" : [ "", "" ],
									"numinlets" : 1,
									"onscreen" : 0,
									"depthbuffer" : 0,
									"patching_rect" : [ 685.0, 124.0, 80.0, 60.0 ],
									"id" : "obj-16",
									"numoutlets" : 2
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "flonum",
									"htextcolor" : [ 0.870588, 0.870588, 0.870588, 1.0 ],
									"triscale" : 0.9,
									"outlettype" : [ "float", "bang" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"bgcolor" : [ 0.866667, 0.866667, 0.866667, 1.0 ],
									"patching_rect" : [ 851.0, 272.0, 35.0, 17.0 ],
									"id" : "obj-17",
									"numoutlets" : 2
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "message",
									"text" : "fade $1",
									"outlettype" : [ "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 2,
									"patching_rect" : [ 851.0, 292.0, 42.0, 15.0 ],
									"id" : "obj-18",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "flonum",
									"htextcolor" : [ 0.870588, 0.870588, 0.870588, 1.0 ],
									"triscale" : 0.9,
									"outlettype" : [ "float", "bang" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"bgcolor" : [ 0.866667, 0.866667, 0.866667, 1.0 ],
									"patching_rect" : [ 809.0, 272.0, 35.0, 17.0 ],
									"id" : "obj-19",
									"numoutlets" : 2
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "message",
									"text" : "tol $1",
									"outlettype" : [ "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 2,
									"patching_rect" : [ 809.0, 292.0, 35.0, 15.0 ],
									"id" : "obj-20",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "flonum",
									"htextcolor" : [ 0.870588, 0.870588, 0.870588, 1.0 ],
									"triscale" : 0.9,
									"outlettype" : [ "float", "bang" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"bgcolor" : [ 0.866667, 0.866667, 0.866667, 1.0 ],
									"patching_rect" : [ 768.0, 272.0, 35.0, 17.0 ],
									"id" : "obj-21",
									"numoutlets" : 2
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "flonum",
									"htextcolor" : [ 0.870588, 0.870588, 0.870588, 1.0 ],
									"triscale" : 0.9,
									"outlettype" : [ "float", "bang" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"bgcolor" : [ 0.866667, 0.866667, 0.866667, 1.0 ],
									"patching_rect" : [ 727.0, 272.0, 35.0, 17.0 ],
									"id" : "obj-22",
									"numoutlets" : 2
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "flonum",
									"htextcolor" : [ 0.870588, 0.870588, 0.870588, 1.0 ],
									"triscale" : 0.9,
									"outlettype" : [ "float", "bang" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"bgcolor" : [ 0.866667, 0.866667, 0.866667, 1.0 ],
									"patching_rect" : [ 686.0, 272.0, 35.0, 17.0 ],
									"id" : "obj-23",
									"numoutlets" : 2
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "flonum",
									"htextcolor" : [ 0.870588, 0.870588, 0.870588, 1.0 ],
									"triscale" : 0.9,
									"outlettype" : [ "float", "bang" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"bgcolor" : [ 0.866667, 0.866667, 0.866667, 1.0 ],
									"patching_rect" : [ 645.0, 272.0, 35.0, 17.0 ],
									"id" : "obj-24",
									"numoutlets" : 2
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "pak color 0. 0. 0. 0.",
									"outlettype" : [ "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 5,
									"patching_rect" : [ 604.0, 292.0, 177.0, 17.0 ],
									"id" : "obj-25",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "jit.chromakey @mode 2",
									"outlettype" : [ "jit_matrix", "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 2,
									"patching_rect" : [ 579.0, 320.0, 116.0, 17.0 ],
									"id" : "obj-26",
									"numoutlets" : 2
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "number",
									"htextcolor" : [ 0.870588, 0.870588, 0.870588, 1.0 ],
									"triscale" : 0.9,
									"outlettype" : [ "int", "bang" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"maximum" : 1024,
									"bgcolor" : [ 0.4, 0.4, 0.8, 1.0 ],
									"patching_rect" : [ 190.0, 139.0, 38.0, 17.0 ],
									"id" : "obj-27",
									"numoutlets" : 2,
									"minimum" : 2
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "message",
									"text" : "dim $1 $1",
									"outlettype" : [ "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 2,
									"patching_rect" : [ 190.0, 160.0, 56.0, 15.0 ],
									"id" : "obj-28",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "message",
									"text" : "read",
									"outlettype" : [ "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 2,
									"patching_rect" : [ 330.0, 160.0, 30.0, 15.0 ],
									"id" : "obj-29",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "switch 3 1",
									"outlettype" : [ "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 4,
									"patching_rect" : [ 335.0, 242.0, 252.0, 17.0 ],
									"id" : "obj-30",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "message",
									"text" : "open",
									"outlettype" : [ "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 2,
									"bgcolor" : [ 0.6, 0.4, 0.6, 1.0 ],
									"patching_rect" : [ 575.0, 185.0, 30.0, 15.0 ],
									"id" : "obj-31",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "jit.dx.grab 512 512",
									"outlettype" : [ "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 575.0, 213.0, 98.0, 17.0 ],
									"id" : "obj-32",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "number",
									"htextcolor" : [ 0.870588, 0.870588, 0.870588, 1.0 ],
									"triscale" : 0.9,
									"outlettype" : [ "int", "bang" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"bgcolor" : [ 0.8, 0.611765, 0.380392, 1.0 ],
									"patching_rect" : [ 404.0, 92.0, 35.0, 17.0 ],
									"id" : "obj-33",
									"numoutlets" : 2
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "sel 32",
									"outlettype" : [ "bang", "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 2,
									"patching_rect" : [ 362.0, 68.0, 38.0, 17.0 ],
									"color" : [ 0.290196, 0.611765, 0.380392, 1.0 ],
									"id" : "obj-34",
									"numoutlets" : 2
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "key",
									"outlettype" : [ "int", "int", "int", "int" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 0,
									"patching_rect" : [ 362.0, 45.0, 40.0, 17.0 ],
									"color" : [ 0.290196, 0.611765, 0.380392, 1.0 ],
									"id" : "obj-35",
									"numoutlets" : 4
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "toggle",
									"outlettype" : [ "int" ],
									"numinlets" : 1,
									"patching_rect" : [ 579.0, 395.0, 28.0, 28.0 ],
									"id" : "obj-36",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "route connected",
									"outlettype" : [ "", "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 579.0, 370.0, 89.0, 17.0 ],
									"color" : [ 0.380392, 0.611765, 0.611765, 1.0 ],
									"id" : "obj-37",
									"numoutlets" : 2
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "message",
									"text" : "port 32005",
									"outlettype" : [ "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 2,
									"bgcolor" : [ 0.380392, 0.611765, 0.611765, 1.0 ],
									"patching_rect" : [ 515.0, 321.0, 62.0, 15.0 ],
									"id" : "obj-38",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "message",
									"text" : "host localhost",
									"outlettype" : [ "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 2,
									"bgcolor" : [ 0.380392, 0.611765, 0.611765, 1.0 ],
									"patching_rect" : [ 441.0, 321.0, 72.0, 15.0 ],
									"id" : "obj-39",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "jit.net.send @host localhost @port 32005",
									"outlettype" : [ "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 2,
									"patching_rect" : [ 579.0, 346.0, 197.0, 17.0 ],
									"id" : "obj-40",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "loadbang",
									"outlettype" : [ "bang" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 248.0, 139.0, 48.0, 17.0 ],
									"id" : "obj-41",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "message",
									"text" : "read ozone.mov",
									"outlettype" : [ "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 2,
									"patching_rect" : [ 248.0, 160.0, 80.0, 15.0 ],
									"id" : "obj-42",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "toggle",
									"outlettype" : [ "int" ],
									"numinlets" : 1,
									"patching_rect" : [ 362.0, 92.0, 15.0, 15.0 ],
									"id" : "obj-43",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "metro 40",
									"outlettype" : [ "bang" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 2,
									"patching_rect" : [ 362.0, 111.0, 52.0, 17.0 ],
									"id" : "obj-44",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "jit.qt.movie 304 304",
									"outlettype" : [ "jit_matrix", "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 362.0, 213.0, 109.0, 17.0 ],
									"id" : "obj-45",
									"numoutlets" : 2
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "connection indicator",
									"fontname" : "Arial",
									"frgb" : [ 0.380392, 0.611765, 0.611765, 1.0 ],
									"fontsize" : 9.0,
									"numinlets" : 1,
									"textcolor" : [ 0.380392, 0.611765, 0.611765, 1.0 ],
									"patching_rect" : [ 608.0, 395.0, 100.0, 17.0 ],
									"id" : "obj-46",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "specify custom host/port",
									"fontname" : "Arial",
									"frgb" : [ 0.380392, 0.611765, 0.611765, 1.0 ],
									"fontsize" : 9.0,
									"numinlets" : 1,
									"textcolor" : [ 0.380392, 0.611765, 0.611765, 1.0 ],
									"patching_rect" : [ 445.0, 307.0, 130.0, 17.0 ],
									"id" : "obj-47",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "Mac live camera input",
									"linecount" : 2,
									"fontname" : "Arial",
									"frgb" : [ 0.6, 0.4, 0.6, 1.0 ],
									"fontsize" : 9.0,
									"numinlets" : 1,
									"textcolor" : [ 0.6, 0.4, 0.6, 1.0 ],
									"patching_rect" : [ 472.0, 159.0, 68.0, 27.0 ],
									"id" : "obj-48",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "SPACE = toggle on/off",
									"fontname" : "Arial",
									"frgb" : [ 0.290196, 0.611765, 0.380392, 1.0 ],
									"fontsize" : 9.0,
									"numinlets" : 1,
									"textcolor" : [ 0.290196, 0.611765, 0.380392, 1.0 ],
									"patching_rect" : [ 403.0, 47.0, 119.0, 17.0 ],
									"id" : "obj-49",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "update speed in ms (default 25 frames/sec or every 40ms)",
									"linecount" : 3,
									"fontname" : "Arial",
									"frgb" : [ 0.8, 0.611765, 0.380392, 1.0 ],
									"fontsize" : 9.0,
									"numinlets" : 1,
									"textcolor" : [ 0.8, 0.611765, 0.380392, 1.0 ],
									"patching_rect" : [ 440.0, 82.0, 111.0, 38.0 ],
									"id" : "obj-50",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "TEXTURE DIMENSIONS: must be power of two and (in theory) can be altered dynamically. In practice we found that Nvidia hardware allows any dimensions/ratios that are not multiples of 64, otherwise the texture would be incorrectly mapped. On ATI hardware, only resolutions that were power of 2 (and in any XY ratio) were visible, while other sizes resulted in garbage textures and/or mesh corruption. Solution? Don't do textures that are not power of 2.",
									"linecount" : 7,
									"fontname" : "Arial",
									"frgb" : [ 0.4, 0.4, 0.8, 1.0 ],
									"fontsize" : 9.0,
									"numinlets" : 1,
									"textcolor" : [ 0.4, 0.4, 0.8, 1.0 ],
									"patching_rect" : [ 12.0, 16.0, 325.0, 79.0 ],
									"id" : "obj-51",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "PRESETS",
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 67.0, 125.0, 52.0, 17.0 ],
									"id" : "obj-52",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "video playback",
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 42.0, 160.0, 129.0, 17.0 ],
									"id" : "obj-53",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "manually select chromakey color by clicking on the image or on the color palette below",
									"linecount" : 3,
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 768.0, 150.0, 142.0, 38.0 ],
									"id" : "obj-54",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "WINDOWS USERS: make sure to run the Unity binary with -force-opengl flag. Currently Unity3d does support Direct3D as default renderer but lacks a way of passing Direct3D-compatible texture pointer to plugin. Hence, the current implementation relies upon OpenGL. If you wish to use implementation that does not require a plugin, use JitReceiveTextureAlt.cs file in the Unity's Plugins/Alt folder. Please note that while this version is universal, it is also a lot less efficient.",
									"linecount" : 8,
									"fontname" : "Arial",
									"frgb" : [ 0.4, 0.4, 0.8, 1.0 ],
									"fontsize" : 9.0,
									"numinlets" : 1,
									"textcolor" : [ 0.4, 0.4, 0.8, 1.0 ],
									"patching_rect" : [ 12.0, 233.0, 293.0, 89.0 ],
									"id" : "obj-55",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "manual trigger",
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 388.0, 161.0, 71.0, 17.0 ],
									"id" : "obj-56",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "CHROMAKEY and CASTING SHADOWS (Unity Pro only): Sphere casts a shadow while Cube (depending upon what material it is currently using) may not always. Likewise, chromakey effect only works when cube is shadowless. This is because in this demo Cube's material can be toggled between transparent and diffuse using custom commands. The transparent material is useful for allowing chromakeyed texture to produce transparent areas. To change Cube material, use custom command found in the Max->Unity_Control patcher.",
									"linecount" : 6,
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 15.0, 349.0, 416.0, 69.0 ],
									"id" : "obj-57",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "1 = video 2 = Mac webcam 3 = Win32 webcam -->",
									"linecount" : 3,
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 222.0, 190.0, 100.0, 38.0 ],
									"id" : "obj-58",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "panel",
									"rounded" : 10,
									"bordercolor" : [ 1.0, 1.0, 1.0, 1.0 ],
									"border" : 1,
									"numinlets" : 1,
									"bgcolor" : [ 0.631373, 0.835294, 0.458824, 1.0 ],
									"patching_rect" : [ 12.0, 114.0, 160.0, 107.0 ],
									"id" : "obj-59",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "panel",
									"rounded" : 10,
									"bordercolor" : [ 1.0, 1.0, 1.0, 1.0 ],
									"border" : 1,
									"numinlets" : 1,
									"bgcolor" : [ 0.866667, 0.556863, 0.560784, 1.0 ],
									"patching_rect" : [ 12.0, 344.0, 424.0, 87.0 ],
									"id" : "obj-60",
									"numoutlets" : 0
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"source" : [ "obj-17", 0 ],
									"destination" : [ "obj-18", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-19", 0 ],
									"destination" : [ "obj-20", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-21", 0 ],
									"destination" : [ "obj-25", 4 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-12", 2 ],
									"destination" : [ "obj-21", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-22", 0 ],
									"destination" : [ "obj-25", 3 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-12", 1 ],
									"destination" : [ "obj-22", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-23", 0 ],
									"destination" : [ "obj-25", 2 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-12", 0 ],
									"destination" : [ "obj-23", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-13", 0 ],
									"destination" : [ "obj-12", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-14", 0 ],
									"destination" : [ "obj-13", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-15", 0 ],
									"destination" : [ "obj-14", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-30", 0 ],
									"destination" : [ "obj-16", 0 ],
									"hidden" : 0,
									"midpoints" : [ 344.5, 264.0, 679.0, 264.0, 679.0, 120.0, 694.5, 120.0 ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-24", 0 ],
									"destination" : [ "obj-25", 1 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-37", 0 ],
									"destination" : [ "obj-36", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-11", 2 ],
									"destination" : [ "obj-36", 0 ],
									"hidden" : 1,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-40", 0 ],
									"destination" : [ "obj-37", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-26", 0 ],
									"destination" : [ "obj-40", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-38", 0 ],
									"destination" : [ "obj-40", 0 ],
									"hidden" : 0,
									"midpoints" : [ 524.5, 341.0, 588.5, 341.0 ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-39", 0 ],
									"destination" : [ "obj-40", 0 ],
									"hidden" : 0,
									"midpoints" : [ 450.5, 341.0, 588.5, 341.0 ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-18", 0 ],
									"destination" : [ "obj-26", 0 ],
									"hidden" : 0,
									"midpoints" : [ 860.5, 314.0, 588.5, 314.0 ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-20", 0 ],
									"destination" : [ "obj-26", 0 ],
									"hidden" : 0,
									"midpoints" : [ 818.5, 314.0, 588.5, 314.0 ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-25", 0 ],
									"destination" : [ "obj-26", 0 ],
									"hidden" : 0,
									"midpoints" : [ 613.5, 314.0, 588.5, 314.0 ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-30", 0 ],
									"destination" : [ "obj-26", 0 ],
									"hidden" : 0,
									"midpoints" : [ 344.5, 264.0, 588.5, 264.0 ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-32", 0 ],
									"destination" : [ "obj-30", 3 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-31", 0 ],
									"destination" : [ "obj-32", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-44", 0 ],
									"destination" : [ "obj-32", 0 ],
									"hidden" : 0,
									"midpoints" : [ 371.5, 207.0, 584.5, 207.0 ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-28", 0 ],
									"destination" : [ "obj-32", 0 ],
									"hidden" : 0,
									"midpoints" : [ 199.5, 180.0, 367.0, 180.0, 367.0, 207.0, 584.5, 207.0 ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-7", 0 ],
									"destination" : [ "obj-32", 0 ],
									"hidden" : 0,
									"midpoints" : [ 381.0, 207.0, 584.5, 207.0 ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-10", 0 ],
									"destination" : [ "obj-30", 2 ],
									"hidden" : 0,
									"midpoints" : [ 483.5, 236.0, 499.833344, 236.0 ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-9", 0 ],
									"destination" : [ "obj-10", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-44", 0 ],
									"destination" : [ "obj-10", 0 ],
									"hidden" : 0,
									"midpoints" : [ 371.5, 207.0, 483.5, 207.0 ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-28", 0 ],
									"destination" : [ "obj-10", 0 ],
									"hidden" : 0,
									"midpoints" : [ 199.5, 180.0, 367.0, 180.0, 367.0, 207.0, 483.5, 207.0 ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-7", 0 ],
									"destination" : [ "obj-10", 0 ],
									"hidden" : 0,
									"midpoints" : [ 381.0, 207.0, 483.5, 207.0 ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-45", 0 ],
									"destination" : [ "obj-30", 1 ],
									"hidden" : 0,
									"midpoints" : [ 371.5, 236.0, 422.166656, 236.0 ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-33", 0 ],
									"destination" : [ "obj-44", 1 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-29", 0 ],
									"destination" : [ "obj-45", 0 ],
									"hidden" : 0,
									"midpoints" : [ 339.5, 180.0, 371.5, 180.0 ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-42", 0 ],
									"destination" : [ "obj-45", 0 ],
									"hidden" : 0,
									"midpoints" : [ 257.5, 180.0, 371.5, 180.0 ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-44", 0 ],
									"destination" : [ "obj-45", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-28", 0 ],
									"destination" : [ "obj-45", 0 ],
									"hidden" : 0,
									"midpoints" : [ 199.5, 180.0, 371.5, 180.0 ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-7", 0 ],
									"destination" : [ "obj-45", 0 ],
									"hidden" : 0,
									"midpoints" : [ 381.0, 207.0, 371.5, 207.0 ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-43", 0 ],
									"destination" : [ "obj-44", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-34", 0 ],
									"destination" : [ "obj-43", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-35", 0 ],
									"destination" : [ "obj-34", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-3", 0 ],
									"destination" : [ "obj-30", 0 ],
									"hidden" : 0,
									"midpoints" : [ 333.5, 237.0, 344.5, 237.0 ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-41", 0 ],
									"destination" : [ "obj-42", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-27", 0 ],
									"destination" : [ "obj-28", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-4", 0 ],
									"destination" : [ "obj-27", 0 ],
									"hidden" : 1,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-11", 2 ],
									"destination" : [ "obj-27", 0 ],
									"hidden" : 1,
									"midpoints" : [  ]
								}

							}
 ]
					}
,
					"saved_object_attributes" : 					{
						"default_fontsize" : 12.0,
						"fontname" : "Arial",
						"globalpatchername" : "",
						"fontface" : 0,
						"fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial"
					}

				}

			}
, 			{
				"box" : 				{
					"maxclass" : "ubutton",
					"outlettype" : [ "bang", "bang", "", "int" ],
					"handoff" : "",
					"numinlets" : 1,
					"patching_rect" : [ 181.0, 221.0, 117.0, 18.0 ],
					"id" : "obj-3",
					"numoutlets" : 4
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "ubutton",
					"outlettype" : [ "bang", "bang", "", "int" ],
					"handoff" : "",
					"hilite" : 0,
					"numinlets" : 1,
					"patching_rect" : [ 539.0, 175.0, 81.0, 67.0 ],
					"id" : "obj-4",
					"numoutlets" : 4
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "message",
					"text" : ";\rmax launchbrowser http://disis.music.vt.edu",
					"linecount" : 2,
					"outlettype" : [ "" ],
					"fontname" : "Arial",
					"hidden" : 1,
					"fontsize" : 9.0,
					"numinlets" : 2,
					"patching_rect" : [ 181.0, 243.0, 216.0, 25.0 ],
					"id" : "obj-5",
					"numoutlets" : 1
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "With thanks to Denis Gracanin",
					"fontname" : "Arial",
					"fontsize" : 9.0,
					"numinlets" : 1,
					"patching_rect" : [ 179.0, 128.0, 253.0, 17.0 ],
					"id" : "obj-6",
					"numoutlets" : 0
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "Virginia Tech Department of Music",
					"fontname" : "Arial",
					"fontsize" : 9.0,
					"numinlets" : 1,
					"patching_rect" : [ 179.0, 152.0, 224.0, 17.0 ],
					"id" : "obj-7",
					"numoutlets" : 0
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "Collaborative for Creative Technologies in the Arts and Design",
					"fontname" : "Arial",
					"fontsize" : 9.0,
					"numinlets" : 1,
					"patching_rect" : [ 179.0, 182.0, 296.0, 17.0 ],
					"id" : "obj-8",
					"numoutlets" : 0
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "Ji-Sun Kim <hideaway@vt.edu>",
					"fontname" : "Arial",
					"fontsize" : 9.0,
					"numinlets" : 1,
					"patching_rect" : [ 179.0, 98.0, 253.0, 17.0 ],
					"id" : "obj-9",
					"numoutlets" : 0
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "Keith Wooldridge <kawoold@vt.edu>",
					"fontname" : "Arial",
					"fontsize" : 9.0,
					"numinlets" : 1,
					"patching_rect" : [ 179.0, 113.0, 253.0, 17.0 ],
					"id" : "obj-10",
					"numoutlets" : 0
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "Ivica Ico Bukvic <ico@vt.edu> <http://ico.bukvic.net>",
					"fontname" : "Arial",
					"fontsize" : 9.0,
					"numinlets" : 1,
					"patching_rect" : [ 179.0, 83.0, 253.0, 17.0 ],
					"id" : "obj-11",
					"numoutlets" : 0
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "fpic",
					"numinlets" : 1,
					"patching_rect" : [ 538.0, 174.0, 83.0, 69.0 ],
					"id" : "obj-12",
					"numoutlets" : 0,
					"pic" : "disis-small.jpg"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "newobj",
					"text" : "p Unity->Max_Control",
					"fontname" : "Arial",
					"fontsize" : 9.0,
					"numinlets" : 0,
					"patching_rect" : [ 505.0, 109.0, 113.0, 17.0 ],
					"id" : "obj-13",
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"rect" : [ 22.0, 439.0, 510.0, 344.0 ],
						"bglocked" : 0,
						"defrect" : [ 22.0, 439.0, 510.0, 344.0 ],
						"openrect" : [ 0.0, 0.0, 0.0, 0.0 ],
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 0,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 0,
						"toolbarvisible" : 1,
						"boxanimatetime" : 200,
						"imprint" : 0,
						"metadata" : [  ],
						"boxes" : [ 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "x y z",
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 134.0, 190.0, 30.0, 17.0 ],
									"id" : "obj-1",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "route by operation (in this case rotate)",
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 63.0, 168.0, 183.0, 17.0 ],
									"id" : "obj-2",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "route r",
									"outlettype" : [ "", "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 19.0, 166.0, 42.0, 17.0 ],
									"id" : "obj-3",
									"numoutlets" : 2
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "flonum",
									"htextcolor" : [ 0.870588, 0.870588, 0.870588, 1.0 ],
									"triscale" : 0.9,
									"outlettype" : [ "float", "bang" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"bgcolor" : [ 0.380392, 0.611765, 0.611765, 1.0 ],
									"patching_rect" : [ 121.0, 210.0, 49.0, 17.0 ],
									"id" : "obj-4",
									"numoutlets" : 2
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "flonum",
									"htextcolor" : [ 0.870588, 0.870588, 0.870588, 1.0 ],
									"triscale" : 0.9,
									"outlettype" : [ "float", "bang" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"bgcolor" : [ 0.380392, 0.611765, 0.611765, 1.0 ],
									"patching_rect" : [ 70.0, 210.0, 49.0, 17.0 ],
									"id" : "obj-5",
									"numoutlets" : 2
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "flonum",
									"htextcolor" : [ 0.870588, 0.870588, 0.870588, 1.0 ],
									"triscale" : 0.9,
									"outlettype" : [ "float", "bang" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"bgcolor" : [ 0.380392, 0.611765, 0.611765, 1.0 ],
									"patching_rect" : [ 19.0, 210.0, 49.0, 17.0 ],
									"id" : "obj-6",
									"numoutlets" : 2
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "unpack f f f",
									"outlettype" : [ "float", "float", "float" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 19.0, 188.0, 113.0, 17.0 ],
									"id" : "obj-7",
									"numoutlets" : 3
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "route Cube",
									"outlettype" : [ "", "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 19.0, 145.0, 58.0, 17.0 ],
									"id" : "obj-8",
									"numoutlets" : 2
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "netreceive 32000",
									"outlettype" : [ "", "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 0,
									"patching_rect" : [ 19.0, 124.0, 91.0, 17.0 ],
									"id" : "obj-9",
									"numoutlets" : 2
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "IMPORTANT! This implementation uses TCP/IP protocol through Olaf Matthes' netsend/netreceive external (also available from http://www.akustische-kunst.org/maxmsp/).",
									"linecount" : 2,
									"fontname" : "Arial Black",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 18.0, 20.0, 467.0, 32.0 ],
									"id" : "obj-10",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "FORMAT: dictated by the Unity script. See JitMessenger.cs for more info.",
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 19.0, 71.0, 349.0, 17.0 ],
									"id" : "obj-11",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "JitSend.cs uses \"Packet Treshold\" value (default 100) to drop queued packets if the queue exceeds 100 in the case of conenction problems. This value is customizable within Unity.",
									"linecount" : 2,
									"fontname" : "Arial",
									"frgb" : [ 0.4, 0.4, 0.8, 1.0 ],
									"fontsize" : 9.0,
									"numinlets" : 1,
									"textcolor" : [ 0.4, 0.4, 0.8, 1.0 ],
									"patching_rect" : [ 19.0, 91.0, 435.0, 27.0 ],
									"id" : "obj-12",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "receive port",
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 112.0, 126.0, 100.0, 17.0 ],
									"id" : "obj-13",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "DATA FLOW: JitMessenger -> JitSend (commonly linked to \"Main Camera)\" -> Max",
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 19.0, 57.0, 389.0, 17.0 ],
									"id" : "obj-14",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "route by object name",
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 79.0, 147.0, 109.0, 17.0 ],
									"id" : "obj-15",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "Please note in this tutorial the output will only occur if the Cube's rotation is altered due to IsSleeping() test within JitMessenger script that minimizes redundant otput. Therefore, for this example to work, you need to enable \"toggle relative rotation\" in the Max->Unity_Control patcher.",
									"linecount" : 4,
									"fontname" : "Arial",
									"frgb" : [ 0.4, 0.4, 0.8, 1.0 ],
									"fontsize" : 9.0,
									"numinlets" : 1,
									"textcolor" : [ 0.4, 0.4, 0.8, 1.0 ],
									"patching_rect" : [ 18.0, 233.0, 394.0, 48.0 ],
									"id" : "obj-16",
									"numoutlets" : 0
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"source" : [ "obj-7", 2 ],
									"destination" : [ "obj-4", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-7", 1 ],
									"destination" : [ "obj-5", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-7", 0 ],
									"destination" : [ "obj-6", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-3", 0 ],
									"destination" : [ "obj-7", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-8", 0 ],
									"destination" : [ "obj-3", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-9", 0 ],
									"destination" : [ "obj-8", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
 ]
					}
,
					"saved_object_attributes" : 					{
						"default_fontsize" : 12.0,
						"fontname" : "Arial",
						"globalpatchername" : "",
						"fontface" : 0,
						"fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial"
					}

				}

			}
, 			{
				"box" : 				{
					"maxclass" : "newobj",
					"text" : "p Max->Unity_Control",
					"fontname" : "Arial",
					"fontsize" : 9.0,
					"numinlets" : 0,
					"patching_rect" : [ 505.0, 89.0, 113.0, 17.0 ],
					"id" : "obj-14",
					"numoutlets" : 0,
					"patcher" : 					{
						"fileversion" : 1,
						"rect" : [ 14.0, 399.0, 951.0, 367.0 ],
						"bglocked" : 0,
						"defrect" : [ 14.0, 399.0, 951.0, 367.0 ],
						"openrect" : [ 0.0, 0.0, 0.0, 0.0 ],
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 0,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 0,
						"toolbarvisible" : 1,
						"boxanimatetime" : 200,
						"imprint" : 0,
						"metadata" : [  ],
						"boxes" : [ 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "Open the patcher below for physics simulation examples",
									"linecount" : 3,
									"fontname" : "Arial",
									"frgb" : [ 0.380392, 0.611765, 0.611765, 1.0 ],
									"fontsize" : 9.0,
									"numinlets" : 1,
									"textcolor" : [ 0.380392, 0.611765, 0.611765, 1.0 ],
									"patching_rect" : [ 688.0, 195.0, 101.0, 38.0 ],
									"id" : "obj-1",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "p physics_simulation",
									"outlettype" : [ "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 0,
									"patching_rect" : [ 687.0, 235.0, 104.0, 17.0 ],
									"color" : [ 0.380392, 0.611765, 0.611765, 1.0 ],
									"id" : "obj-2",
									"numoutlets" : 1,
									"patcher" : 									{
										"fileversion" : 1,
										"rect" : [ 553.0, 253.0, 346.0, 468.0 ],
										"bglocked" : 0,
										"defrect" : [ 553.0, 253.0, 346.0, 468.0 ],
										"openrect" : [ 0.0, 0.0, 0.0, 0.0 ],
										"openinpresentation" : 0,
										"default_fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial",
										"gridonopen" : 0,
										"gridsize" : [ 15.0, 15.0 ],
										"gridsnaponopen" : 0,
										"toolbarvisible" : 1,
										"boxanimatetime" : 200,
										"imprint" : 0,
										"metadata" : [  ],
										"boxes" : [ 											{
												"box" : 												{
													"maxclass" : "comment",
													"text" : "Click here to make it rain cats'n'dogs -->",
													"linecount" : 3,
													"fontname" : "Arial",
													"frgb" : [ 0.380392, 0.611765, 0.611765, 1.0 ],
													"fontsize" : 9.0,
													"numinlets" : 1,
													"textcolor" : [ 0.380392, 0.611765, 0.611765, 1.0 ],
													"patching_rect" : [ 47.0, 295.0, 74.0, 38.0 ],
													"id" : "obj-1",
													"numoutlets" : 0
												}

											}
, 											{
												"box" : 												{
													"maxclass" : "comment",
													"text" : "Custom 5 (c 5) function destroys all cats and dogs.",
													"fontname" : "Arial",
													"fontsize" : 9.0,
													"numinlets" : 1,
													"patching_rect" : [ 25.0, 245.0, 288.0, 17.0 ],
													"id" : "obj-2",
													"numoutlets" : 0
												}

											}
, 											{
												"box" : 												{
													"maxclass" : "comment",
													"text" : "Custom 4 (c 4) function spawns \"Dogs\" (cubes) above the camera view that due to their mass and global gravity parameters drop downwards. Because they are also made out of a \"Bouncy\" material, they also bounce around before the friction and gravity gets the best of them.",
													"linecount" : 5,
													"fontname" : "Arial",
													"fontsize" : 9.0,
													"numinlets" : 1,
													"patching_rect" : [ 25.0, 175.0, 288.0, 58.0 ],
													"id" : "obj-3",
													"numoutlets" : 0
												}

											}
, 											{
												"box" : 												{
													"maxclass" : "toggle",
													"outlettype" : [ "int" ],
													"numinlets" : 1,
													"patching_rect" : [ 123.0, 318.0, 15.0, 15.0 ],
													"id" : "obj-4",
													"numoutlets" : 1
												}

											}
, 											{
												"box" : 												{
													"maxclass" : "newobj",
													"text" : "metro 250",
													"outlettype" : [ "bang" ],
													"fontname" : "Arial",
													"fontsize" : 9.0,
													"numinlets" : 2,
													"patching_rect" : [ 123.0, 338.0, 58.0, 17.0 ],
													"id" : "obj-5",
													"numoutlets" : 1
												}

											}
, 											{
												"box" : 												{
													"maxclass" : "comment",
													"text" : "Custom 3 (c 3) function spawns \"Cats\" (spheres) above the camera view that due to their mass and global gravity parameters drop downwards. Because they are made out of a \"Bouncy\" material, they also bounce around before the friction and gravity gets the bets of them.",
													"linecount" : 5,
													"fontname" : "Arial",
													"fontsize" : 9.0,
													"numinlets" : 1,
													"patching_rect" : [ 25.0, 106.0, 287.0, 58.0 ],
													"id" : "obj-6",
													"numoutlets" : 0
												}

											}
, 											{
												"box" : 												{
													"maxclass" : "comment",
													"text" : "PHYSICS SIMULATION",
													"fontname" : "Arial Black",
													"fontsize" : 9.0,
													"numinlets" : 1,
													"patching_rect" : [ 112.0, 19.0, 120.0, 19.0 ],
													"id" : "obj-7",
													"numoutlets" : 0
												}

											}
, 											{
												"box" : 												{
													"maxclass" : "message",
													"text" : "Main_Camera c 5",
													"outlettype" : [ "" ],
													"fontname" : "Arial",
													"fontsize" : 9.0,
													"numinlets" : 2,
													"bgcolor" : [ 0.380392, 0.611765, 0.611765, 1.0 ],
													"patching_rect" : [ 213.0, 363.0, 88.0, 15.0 ],
													"id" : "obj-8",
													"numoutlets" : 1
												}

											}
, 											{
												"box" : 												{
													"maxclass" : "message",
													"text" : "Main_Camera c 4",
													"outlettype" : [ "" ],
													"fontname" : "Arial",
													"fontsize" : 9.0,
													"numinlets" : 2,
													"bgcolor" : [ 0.380392, 0.611765, 0.611765, 1.0 ],
													"patching_rect" : [ 123.0, 363.0, 88.0, 15.0 ],
													"id" : "obj-9",
													"numoutlets" : 1
												}

											}
, 											{
												"box" : 												{
													"maxclass" : "message",
													"text" : "Main_Camera c 3",
													"outlettype" : [ "" ],
													"fontname" : "Arial",
													"fontsize" : 9.0,
													"numinlets" : 2,
													"bgcolor" : [ 0.380392, 0.611765, 0.611765, 1.0 ],
													"patching_rect" : [ 33.0, 363.0, 88.0, 15.0 ],
													"id" : "obj-10",
													"numoutlets" : 1
												}

											}
, 											{
												"box" : 												{
													"maxclass" : "outlet",
													"numinlets" : 1,
													"patching_rect" : [ 123.0, 386.0, 15.0, 15.0 ],
													"id" : "obj-11",
													"numoutlets" : 0,
													"comment" : ""
												}

											}
, 											{
												"box" : 												{
													"maxclass" : "comment",
													"text" : "In this example, the following custom functions create new objects. As such, they require that the spawning script within the Unity3D is tied to something that is persistent, such as the Main_Camera GameObject.",
													"linecount" : 4,
													"fontname" : "Arial",
													"fontsize" : 9.0,
													"numinlets" : 1,
													"patching_rect" : [ 25.0, 49.0, 287.0, 48.0 ],
													"id" : "obj-12",
													"numoutlets" : 0
												}

											}
 ],
										"lines" : [ 											{
												"patchline" : 												{
													"source" : [ "obj-9", 0 ],
													"destination" : [ "obj-11", 0 ],
													"hidden" : 0,
													"midpoints" : [  ]
												}

											}
, 											{
												"patchline" : 												{
													"source" : [ "obj-10", 0 ],
													"destination" : [ "obj-11", 0 ],
													"hidden" : 0,
													"midpoints" : [ 42.5, 382.0, 132.0, 382.0 ]
												}

											}
, 											{
												"patchline" : 												{
													"source" : [ "obj-8", 0 ],
													"destination" : [ "obj-11", 0 ],
													"hidden" : 0,
													"midpoints" : [ 222.5, 382.0, 132.0, 382.0 ]
												}

											}
, 											{
												"patchline" : 												{
													"source" : [ "obj-5", 0 ],
													"destination" : [ "obj-9", 0 ],
													"hidden" : 0,
													"midpoints" : [  ]
												}

											}
, 											{
												"patchline" : 												{
													"source" : [ "obj-4", 0 ],
													"destination" : [ "obj-5", 0 ],
													"hidden" : 0,
													"midpoints" : [  ]
												}

											}
, 											{
												"patchline" : 												{
													"source" : [ "obj-5", 0 ],
													"destination" : [ "obj-10", 0 ],
													"hidden" : 0,
													"midpoints" : [ 132.5, 359.0, 42.5, 359.0 ]
												}

											}
 ]
									}
,
									"saved_object_attributes" : 									{
										"default_fontsize" : 12.0,
										"fontname" : "Arial",
										"globalpatchername" : "",
										"fontface" : 0,
										"fontsize" : 12.0,
										"default_fontface" : 0,
										"default_fontname" : "Arial"
									}

								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "line 0. 40",
									"outlettype" : [ "", "", "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 3,
									"patching_rect" : [ 711.0, 131.0, 53.0, 17.0 ],
									"id" : "obj-3",
									"numoutlets" : 3
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "append 500",
									"outlettype" : [ "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 711.0, 111.0, 61.0, 17.0 ],
									"id" : "obj-4",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "line 0. 40",
									"outlettype" : [ "", "", "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 3,
									"patching_rect" : [ 645.0, 131.0, 53.0, 17.0 ],
									"id" : "obj-5",
									"numoutlets" : 3
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "append 500",
									"outlettype" : [ "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 645.0, 111.0, 61.0, 17.0 ],
									"id" : "obj-6",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "line 0. 40",
									"outlettype" : [ "", "", "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 3,
									"patching_rect" : [ 579.0, 131.0, 53.0, 17.0 ],
									"id" : "obj-7",
									"numoutlets" : 3
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "append 500",
									"outlettype" : [ "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 579.0, 111.0, 61.0, 17.0 ],
									"id" : "obj-8",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "Change Cube's material from shadowless transparent (0) to diffuse with shadow (1)",
									"linecount" : 4,
									"fontname" : "Arial",
									"frgb" : [ 0.380392, 0.611765, 0.611765, 1.0 ],
									"fontsize" : 9.0,
									"numinlets" : 1,
									"textcolor" : [ 0.380392, 0.611765, 0.611765, 1.0 ],
									"patching_rect" : [ 820.0, 263.0, 115.0, 48.0 ],
									"id" : "obj-9",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "message",
									"text" : "Cube c 2 1",
									"outlettype" : [ "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 2,
									"bgcolor" : [ 0.380392, 0.611765, 0.611765, 1.0 ],
									"patching_rect" : [ 854.0, 236.0, 57.0, 15.0 ],
									"id" : "obj-10",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "message",
									"text" : "Cube c 2 0",
									"outlettype" : [ "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 2,
									"bgcolor" : [ 0.380392, 0.611765, 0.611765, 1.0 ],
									"patching_rect" : [ 796.0, 236.0, 57.0, 15.0 ],
									"id" : "obj-11",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "DATA FLOW: Max -> JitReceive (commonly linked to the \"Main_Camera)\" -> Target Object",
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 12.0, 36.0, 426.0, 17.0 ],
									"id" : "obj-12",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "port/host is customizable",
									"fontname" : "Arial",
									"frgb" : [ 0.6, 0.4, 0.6, 1.0 ],
									"fontsize" : 9.0,
									"numinlets" : 1,
									"textcolor" : [ 0.6, 0.4, 0.6, 1.0 ],
									"patching_rect" : [ 238.0, 221.0, 134.0, 17.0 ],
									"id" : "obj-13",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "2nd argument refers to the custom function defined inside JitCustomEvents",
									"linecount" : 4,
									"fontname" : "Arial",
									"frgb" : [ 0.8, 0.611765, 0.380392, 1.0 ],
									"fontsize" : 9.0,
									"numinlets" : 1,
									"textcolor" : [ 0.8, 0.611765, 0.380392, 1.0 ],
									"patching_rect" : [ 578.0, 263.0, 112.0, 48.0 ],
									"id" : "obj-14",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "pack i i i",
									"outlettype" : [ "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 3,
									"patching_rect" : [ 579.0, 152.0, 143.0, 17.0 ],
									"id" : "obj-15",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "random 255",
									"outlettype" : [ "int" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 2,
									"patching_rect" : [ 711.0, 91.0, 64.0, 17.0 ],
									"id" : "obj-16",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "random 255",
									"outlettype" : [ "int" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 2,
									"patching_rect" : [ 645.0, 91.0, 64.0, 17.0 ],
									"id" : "obj-17",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "toggle",
									"outlettype" : [ "int" ],
									"numinlets" : 1,
									"patching_rect" : [ 579.0, 43.0, 15.0, 15.0 ],
									"id" : "obj-18",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "metro 500",
									"outlettype" : [ "bang" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 2,
									"patching_rect" : [ 579.0, 63.0, 58.0, 17.0 ],
									"id" : "obj-19",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "toggle custom event (random color change)",
									"linecount" : 2,
									"fontname" : "Arial",
									"frgb" : [ 0.4, 0.4, 0.8, 1.0 ],
									"fontsize" : 9.0,
									"numinlets" : 1,
									"textcolor" : [ 0.4, 0.4, 0.8, 1.0 ],
									"patching_rect" : [ 597.0, 34.0, 113.0, 27.0 ],
									"id" : "obj-20",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "random 255",
									"outlettype" : [ "int" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 2,
									"patching_rect" : [ 579.0, 91.0, 64.0, 17.0 ],
									"id" : "obj-21",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "message",
									"text" : "Sphere c 1 $1 $2 $3",
									"outlettype" : [ "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 2,
									"bgcolor" : [ 0.8, 0.611765, 0.380392, 1.0 ],
									"patching_rect" : [ 579.0, 236.0, 103.0, 15.0 ],
									"id" : "obj-22",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "toggle",
									"outlettype" : [ "int" ],
									"numinlets" : 1,
									"patching_rect" : [ 452.0, 100.0, 15.0, 15.0 ],
									"id" : "obj-23",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "metro 40",
									"outlettype" : [ "bang" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 2,
									"patching_rect" : [ 452.0, 120.0, 52.0, 17.0 ],
									"id" : "obj-24",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "toggle random absolute Y translation",
									"linecount" : 2,
									"fontname" : "Arial",
									"frgb" : [ 0.4, 0.4, 0.8, 1.0 ],
									"fontsize" : 9.0,
									"numinlets" : 1,
									"textcolor" : [ 0.4, 0.4, 0.8, 1.0 ],
									"patching_rect" : [ 470.0, 91.0, 106.0, 27.0 ],
									"id" : "obj-25",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "expr $f1/50.+0.313",
									"outlettype" : [ "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 452.0, 160.0, 104.0, 17.0 ],
									"id" : "obj-26",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "random 10",
									"outlettype" : [ "int" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 2,
									"patching_rect" : [ 452.0, 140.0, 58.0, 17.0 ],
									"id" : "obj-27",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "message",
									"text" : "Cube M -0.69 $1 -2.17",
									"outlettype" : [ "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 2,
									"patching_rect" : [ 452.0, 236.0, 115.0, 15.0 ],
									"id" : "obj-28",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "(may have to click twice)",
									"fontname" : "Arial",
									"frgb" : [ 0.6, 0.4, 0.6, 1.0 ],
									"fontsize" : 9.0,
									"numinlets" : 1,
									"textcolor" : [ 0.6, 0.4, 0.6, 1.0 ],
									"patching_rect" : [ 238.0, 208.0, 133.0, 17.0 ],
									"id" : "obj-29",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "c = custom (array[]) (object-specific custom methods)",
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 12.0, 167.0, 295.0, 17.0 ],
									"id" : "obj-30",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "S = absolute scale (x, y, z) (Jitter space format)",
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 12.0, 153.0, 270.0, 17.0 ],
									"id" : "obj-31",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "s = relative scale (x, y, z)",
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 12.0, 139.0, 251.0, 17.0 ],
									"id" : "obj-32",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "R = absolute rotate (x, y, z) (Jitter space format)",
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 12.0, 125.0, 275.0, 17.0 ],
									"id" : "obj-33",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "r = relative rotate (x, y, z)",
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 12.0, 111.0, 251.0, 17.0 ],
									"id" : "obj-34",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "M = absolute move (x, y, z) (Jitter space format)",
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 12.0, 97.0, 275.0, 17.0 ],
									"id" : "obj-35",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "toggle",
									"outlettype" : [ "int" ],
									"numinlets" : 1,
									"patching_rect" : [ 358.0, 100.0, 15.0, 15.0 ],
									"id" : "obj-36",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "metro 20",
									"outlettype" : [ "bang" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 2,
									"patching_rect" : [ 358.0, 120.0, 52.0, 17.0 ],
									"id" : "obj-37",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "netsend",
									"outlettype" : [ "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 358.0, 291.0, 44.0, 17.0 ],
									"id" : "obj-38",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "message",
									"text" : "connect localhost 32003",
									"outlettype" : [ "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 2,
									"bgcolor" : [ 0.6, 0.4, 0.6, 1.0 ],
									"patching_rect" : [ 235.0, 236.0, 120.0, 15.0 ],
									"id" : "obj-39",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "newobj",
									"text" : "prepend send",
									"outlettype" : [ "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 358.0, 264.0, 68.0, 17.0 ],
									"id" : "obj-40",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "message",
									"text" : "Cube r 0 1 0",
									"outlettype" : [ "" ],
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 2,
									"patching_rect" : [ 358.0, 236.0, 66.0, 15.0 ],
									"id" : "obj-41",
									"numoutlets" : 1
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "IMPORTANT! This implementation uses TCP/IP protocol through Olaf Matthes' netsend/netreceive external (also available from http://www.akustische-kunst.org/maxmsp/).",
									"fontname" : "Arial Black",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 11.0, 14.0, 849.0, 19.0 ],
									"id" : "obj-42",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "Unity understands following events:",
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 12.0, 69.0, 186.0, 17.0 ],
									"id" : "obj-43",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "m = relative move (x, y, z)",
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 12.0, 83.0, 147.0, 17.0 ],
									"id" : "obj-44",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "FORMAT: unity_object_name event parameters[]",
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 12.0, 50.0, 244.0, 17.0 ],
									"id" : "obj-45",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "toggle relative rotation",
									"linecount" : 2,
									"fontname" : "Arial",
									"frgb" : [ 0.4, 0.4, 0.8, 1.0 ],
									"fontsize" : 9.0,
									"numinlets" : 1,
									"textcolor" : [ 0.4, 0.4, 0.8, 1.0 ],
									"patching_rect" : [ 376.0, 91.0, 73.0, 27.0 ],
									"id" : "obj-46",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "first establish connection",
									"fontname" : "Arial",
									"frgb" : [ 0.6, 0.4, 0.6, 1.0 ],
									"fontsize" : 9.0,
									"numinlets" : 1,
									"textcolor" : [ 0.6, 0.4, 0.6, 1.0 ],
									"patching_rect" : [ 238.0, 195.0, 124.0, 17.0 ],
									"id" : "obj-47",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "supports concurrent messages--events are sent into queue and updated on every Update().",
									"linecount" : 4,
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 428.0, 263.0, 130.0, 48.0 ],
									"id" : "obj-48",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "JitReceive.cs uses \"Max Objects\" value (default 1024) to create an array of cached pointers to objects that are being referenced by Max. This is to improve performance as Unity's Find [by name] method can be CPU intensive. For this reason, \"Max Objects\" has to be set to a number larger than the anticipated number of objects that will be controlled from Max.",
									"linecount" : 9,
									"fontname" : "Arial",
									"frgb" : [ 0.4, 0.4, 0.8, 1.0 ],
									"fontsize" : 9.0,
									"numinlets" : 1,
									"textcolor" : [ 0.4, 0.4, 0.8, 1.0 ],
									"patching_rect" : [ 12.0, 195.0, 196.0, 100.0 ],
									"id" : "obj-49",
									"numoutlets" : 0
								}

							}
, 							{
								"box" : 								{
									"maxclass" : "comment",
									"text" : "NB: You could obviously also interpolate colors in Unity3D to minimize network traffic and improve efficiency",
									"linecount" : 6,
									"fontname" : "Arial",
									"fontsize" : 9.0,
									"numinlets" : 1,
									"patching_rect" : [ 778.0, 84.0, 100.0, 69.0 ],
									"id" : "obj-50",
									"numoutlets" : 0
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"source" : [ "obj-3", 0 ],
									"destination" : [ "obj-15", 2 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-4", 0 ],
									"destination" : [ "obj-3", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-16", 0 ],
									"destination" : [ "obj-4", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-19", 0 ],
									"destination" : [ "obj-16", 0 ],
									"hidden" : 0,
									"midpoints" : [ 588.5, 85.0, 720.5, 85.0 ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-5", 0 ],
									"destination" : [ "obj-15", 1 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-6", 0 ],
									"destination" : [ "obj-5", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-17", 0 ],
									"destination" : [ "obj-6", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-19", 0 ],
									"destination" : [ "obj-17", 0 ],
									"hidden" : 0,
									"midpoints" : [ 588.5, 85.0, 654.5, 85.0 ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-15", 0 ],
									"destination" : [ "obj-22", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-7", 0 ],
									"destination" : [ "obj-15", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-8", 0 ],
									"destination" : [ "obj-7", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-21", 0 ],
									"destination" : [ "obj-8", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-19", 0 ],
									"destination" : [ "obj-21", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-18", 0 ],
									"destination" : [ "obj-19", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-26", 0 ],
									"destination" : [ "obj-28", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-27", 0 ],
									"destination" : [ "obj-26", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-24", 0 ],
									"destination" : [ "obj-27", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-23", 0 ],
									"destination" : [ "obj-24", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-40", 0 ],
									"destination" : [ "obj-38", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-39", 0 ],
									"destination" : [ "obj-38", 0 ],
									"hidden" : 0,
									"midpoints" : [ 244.5, 259.0, 350.0, 259.0, 350.0, 287.0, 367.5, 287.0 ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-2", 0 ],
									"destination" : [ "obj-40", 0 ],
									"hidden" : 0,
									"midpoints" : [ 696.5, 259.0, 367.5, 259.0 ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-11", 0 ],
									"destination" : [ "obj-40", 0 ],
									"hidden" : 0,
									"midpoints" : [ 805.5, 259.0, 367.5, 259.0 ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-10", 0 ],
									"destination" : [ "obj-40", 0 ],
									"hidden" : 0,
									"midpoints" : [ 863.5, 259.0, 367.5, 259.0 ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-22", 0 ],
									"destination" : [ "obj-40", 0 ],
									"hidden" : 0,
									"midpoints" : [ 588.5, 259.0, 367.5, 259.0 ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-28", 0 ],
									"destination" : [ "obj-40", 0 ],
									"hidden" : 0,
									"midpoints" : [ 461.5, 259.0, 367.5, 259.0 ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-41", 0 ],
									"destination" : [ "obj-40", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-37", 0 ],
									"destination" : [ "obj-41", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
, 							{
								"patchline" : 								{
									"source" : [ "obj-36", 0 ],
									"destination" : [ "obj-37", 0 ],
									"hidden" : 0,
									"midpoints" : [  ]
								}

							}
 ]
					}
,
					"saved_object_attributes" : 					{
						"default_fontsize" : 12.0,
						"fontname" : "Arial",
						"globalpatchername" : "",
						"fontface" : 0,
						"fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial"
					}

				}

			}
, 			{
				"box" : 				{
					"maxclass" : "fpic",
					"yoffset" : -25.0,
					"numinlets" : 1,
					"patching_rect" : [ 0.0, 8.0, 178.0, 237.0 ],
					"id" : "obj-15",
					"numoutlets" : 0,
					"pic" : "logo.jpg"
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "mu (myu) Max-Unity Interoperability Toolkit",
					"fontname" : "Arial Black",
					"fontsize" : 12.0,
					"numinlets" : 1,
					"patching_rect" : [ 179.0, 62.0, 294.0, 23.0 ],
					"id" : "obj-16",
					"numoutlets" : 0
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "DISIS Interactive Sound & Intermedia Studio",
					"fontname" : "Arial",
					"fontsize" : 9.0,
					"numinlets" : 1,
					"patching_rect" : [ 179.0, 167.0, 224.0, 17.0 ],
					"id" : "obj-17",
					"numoutlets" : 0
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "comment",
					"text" : "http://disis.music.vt.edu",
					"fontname" : "Arial",
					"frgb" : [ 1.0, 1.0, 1.0, 1.0 ],
					"fontsize" : 9.0,
					"numinlets" : 1,
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ],
					"patching_rect" : [ 186.0, 222.0, 108.0, 17.0 ],
					"id" : "obj-18",
					"numoutlets" : 0
				}

			}
, 			{
				"box" : 				{
					"maxclass" : "panel",
					"rounded" : 10,
					"bordercolor" : [ 1.0, 1.0, 1.0, 0.0 ],
					"border" : 1,
					"numinlets" : 1,
					"bgcolor" : [ 0.45098, 0.137255, 0.145098, 1.0 ],
					"patching_rect" : [ 181.0, 221.0, 117.0, 18.0 ],
					"id" : "obj-19",
					"numoutlets" : 0
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"source" : [ "obj-3", 0 ],
					"destination" : [ "obj-5", 0 ],
					"hidden" : 1,
					"midpoints" : [  ]
				}

			}
, 			{
				"patchline" : 				{
					"source" : [ "obj-4", 1 ],
					"destination" : [ "obj-5", 0 ],
					"hidden" : 1,
					"midpoints" : [  ]
				}

			}
 ]
	}

}
